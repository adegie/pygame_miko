import pygame
import time
import random

from pygame.constants import K_LEFT, K_UP, K_DOWN, K_RIGHT

class Bot:


    def __init__(self, poz_x: int, poz_y: int, nazwa: str, kostiumy):
        self.poz_x=poz_x
        self.poz_y=poz_y
        self.nazwa=nazwa
        self.kostiumy=kostiumy
        self.obrazek=kostiumy[0]

        self.kierunek = 0
        self.predkosc = 1
        self.kostium = 0

    
    def ruch(self):
        if (random.randint(1,20) == 13):
            self.kierunek = random.randint(0, 3)

        if self.kierunek == 0:
            self.poz_x+=self.predkosc
        if self.kierunek == 1:
            self.poz_y+=self.predkosc
        if self.kierunek == 2:
            self.poz_x-=self.predkosc
        if self.kierunek == 3:
            self.poz_y-=self.predkosc
    
    def nastepny_kostium(self):
        self.kostium+=1
        if self.kostium>len(self.kostiumy)-1:
            self.kostium=len(self.kostiumy)-1
        self.obrazek = self.kostiumy[self.kostium]

    def kolizja(self, z_kim):
        self.nastepny_kostium()
        

boty = list() # stworz pusta liste o nazwie boty

def rysuj(ekran):
    for b in boty:
        rect=b.obrazek.get_rect(center=(b.poz_x,b.poz_y))
        ekran.blit(b.obrazek, rect)
        # pygame.draw.rect(ekran, pygame.Color(128,128,128,128), rect, width = 1)

def kolizja(bot1: Bot, bot2: Bot) -> bool:
    return abs(bot1.poz_x-bot2.poz_x)<3 and abs(bot1.poz_y - bot2.poz_y)<3

def przelicz():
    for b in boty:
        b.ruch()
        for drugi in boty:
            if (b!=drugi and kolizja(b, drugi)):
                b.kolizja(drugi)


def main():
    pygame.init() # startuje silnik

    ludzik1 = pygame.image.load("ludzik1.png") # ladujemy grafike od ludzika
    statek2 = pygame.image.load("statek.png") # ladujemy grafike od statku
    ekran = pygame.display.set_mode((700, 600)) # inicjalizujemy ekran z jego wielkoscia 600 na 400 pikseli

    czy_dziala = True # ta zmienna kontroluje czy program dalej dziala

    b = Bot(50,50,"Mikolaj",[ludzik1, statek2])
    b2 = Bot(100,100,"Adam", [ludzik1, statek2])
    b3 = Bot(200,200, "M.G", [statek2])

    boty.append(b)
    boty.append(b2)
    boty.append(b3)
    for nowy in range(100):
        boty.append(Bot(random.randint(1,700),random.randint(1,600),"nowy"+str(nowy),[ludzik1, statek2]))

    while czy_dziala: # glowna petla programu

        ekran.fill((255, 255, 255)) # wyczyszczenie ekranu

        przelicz()
        
        rysuj(ekran) # wyrysuj wszystko co ma byc narysowane

        pygame.display.update()

        # obsluga klikniecia zamkniecia okna
        for zdarzenie in pygame.event.get():
            if zdarzenie.type == pygame.QUIT:
                czy_dziala = False
            
        time.sleep(0.01) # opoznienie, FPS'y

    pygame.quit() # zamkniecie pygame



if __name__ == "__main__":
    main()


        # ekran.blit(statek2, (st_x, st_y))
