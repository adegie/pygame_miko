import pygame
import time
import random
from pygame.constants import K_LEFT, K_UP, K_DOWN, K_RIGHT, K_SPACE, K_w, K_a, K_s, K_d, K_TAB

LEWO = 0
PRAWO = 1
GORA = 2
DOL = 3

MYSZKA_GORA = (30,15,15,15)
MYSZKA_LEWO = (15,15,15,30) 
MYSZKA_PRAWO = (45,15,15,30)
MYSZKA_DOL = (30,30,15,15)

class Sterowanie:
    def podaj_kierunek(self) -> int:
        pass
    def stawianie_klocka(self) -> bool:
        pass

class SterowanieKlawiatura(Sterowanie):

    def __init__(self, lewo=K_LEFT, prawo=K_RIGHT, gora=K_UP, dol=K_DOWN, klocek=K_SPACE)-> None:
        super().__init__()
        self.lewo = lewo
        self.prawo = prawo
        self.gora = gora
        self.dol = dol
        self.klocek = klocek

    def podaj_kierunek(self) -> int:
        klawisze = pygame.key.get_pressed()
        if klawisze[self.lewo]:
            return LEWO
        elif klawisze[self.prawo]:
            return PRAWO
        elif klawisze[self.gora]:
            return GORA
        elif klawisze[self.dol]:
            return DOL
        else:
            return None
    
    def stawianie_klocka(self) -> bool:
        return pygame.key.get_pressed()[self.klocek]

class SterowanieMyszka(Sterowanie):
    def myszka_wewnatrz(self,x,y,prostokat):
        px,py,pdlug,pwys = prostokat
        return x>px and x<(px+pdlug) and y>py and y<py+pwys

    def podaj_kierunek(self) -> int:
        if not (pygame.mouse.get_pressed()[0] or pygame.mouse.get_pressed()[2]):
            return None
        myszka_x, myszka_y = pygame.mouse.get_pos()
        if self.myszka_wewnatrz(myszka_x, myszka_y, MYSZKA_LEWO):
            return LEWO
        elif self.myszka_wewnatrz(myszka_x, myszka_y, MYSZKA_PRAWO):
            return PRAWO
        elif self.myszka_wewnatrz(myszka_x, myszka_y, MYSZKA_GORA):
            return GORA
        elif self.myszka_wewnatrz(myszka_x, myszka_y, MYSZKA_DOL):
            return DOL
        else:
            return None
    
    def stawianie_klocka(self) -> bool:
        return pygame.mouse.get_pressed()[2]

class SterowaniePadem(Sterowanie):

    def __init__(self) -> None:
        super().__init__()
        self.controller = pygame.joystick.Joystick(0)
        self.controller.init()

    def podaj_kierunek(self) -> int: 
        if self.controller.get_button(2):
            return LEWO
        elif self.controller.get_button(1):
            return PRAWO
        elif self.controller.get_button(3):
            return GORA
        elif self.controller.get_button(0):
            return DOL
        return None

    def stawianie_klocka(self) -> bool:
        return self.controller.get_button(7)        
        

class Grafika:
    def __init__(self) -> None:
        self.font = pygame.font.SysFont("Arial", 18)

class Gra:    
    def zgon(self):
        self.koparka_x = self.baza_x
        self.koparka_y = self.baza_y
        self.urobek = 0

    def baza(self):
        if self.klocek < 1 and self.urobek > 2:
            self.urobek -= 2
            self.klocek = 1
        self.wynik += self.urobek
        self.urobek = 0
        self.paliwo = 300

    def __init__(self,kolor,sterowanie: Sterowanie) -> None:
        # inicjalizacja mapy
        self.mapa = [[0 for x in range(100)] for y in range(100)]
        for y in range(30,100):
            for x in range(100):
                self.mapa[y][x] = 1


        self.sterowanie = sterowanie

        self.paliwo = 0

        self.klocek = 1

        self.wynik = 0

        self.baza_y = 29

        self.baza_x = random.randint(40,60)

        self.kolor = kolor

        self.koparka_x = self.baza_x
        self.koparka_y = self.baza_y

        self.grawitacja_tick = 0    

        self.kopanie_kierunek = 0

        self.kopanie_tick = 0

        self.urobek = 0


        # inicjalizacja zasobow
        for i in range(10):
            zasob_y = random.randint(42,90)
            zasob_x = random.randint(1,98)
            for rozrost in range(random.randint(6,12)):
                if zasob_y > 0 and zasob_y<100 and zasob_x>0 and zasob_x<100:
                    self.mapa[zasob_y][zasob_x] = 10
                random_kierunek = 0
                if random.randint(0,10) < 7:
                    # zmiana kierunku
                    random_kierunek = random.randint(0,3)
                if random_kierunek == 0:
                    zasob_x+=1
                elif random_kierunek == 1:
                    zasob_x-=1
                elif random_kierunek == 2:
                    zasob_y+=1
                elif random_kierunek == 3:
                    zasob_y-=1



def przelicz(gra: Gra):
    # baza
    if gra.baza_x == gra.koparka_x and gra.baza_y == gra.koparka_y:
        gra.baza()
    
    # wydziobaj miejsce koparki
    gra.mapa[gra.koparka_y][gra.koparka_x] = 0

    
    poprzedni_kierunek_kopania = gra.kopanie_kierunek
    
    nowa_koparka_x = gra.koparka_x
    nowa_koparka_y = gra.koparka_y

    gra.paliwo-=1

    gra.kopanie_kierunek = gra.sterowanie.podaj_kierunek()

    if gra.kopanie_kierunek==LEWO:
        if gra.koparka_x>0:
            nowa_koparka_x=gra.koparka_x-1
    elif gra.kopanie_kierunek==PRAWO:
        if (gra.koparka_x<99):
            nowa_koparka_x=gra.koparka_x+1
    elif gra.kopanie_kierunek==GORA:
        if (gra.koparka_y>1):
            nowa_koparka_y=gra.koparka_y-1
    elif gra.kopanie_kierunek==DOL:
        if (gra.koparka_y<99):
            nowa_koparka_y=gra.koparka_y+1
    else:
        gra.paliwo += 1 # nic nie nacisniete - oddaje paliwo
    
    # stawianie klocka
    if gra.sterowanie.stawianie_klocka() and not gra.kopanie_kierunek==DOL and gra.klocek > 0:
        if gra.kopanie_kierunek == LEWO:
            gra.mapa[gra.koparka_y][gra.koparka_x+1] = 1
        elif gra.kopanie_kierunek == PRAWO:
            gra.mapa[gra.koparka_y][gra.koparka_x-1] = 1
        elif gra.kopanie_kierunek == GORA:
            gra.mapa[gra.koparka_y+1][gra.koparka_x] = 1
        gra.klocek -= 1

    # grawitacja
    if (gra.mapa[gra.koparka_y+1][gra.koparka_x] == 0):
        gra.grawitacja_tick -= 1
        if (gra.grawitacja_tick == 0):
            nowa_koparka_y = nowa_koparka_y+1
            if gra.kopanie_kierunek == GORA:
                gra.kopanie_tick = 5
            gra.grawitacja_tick = 3
    else:
        gra.grawitacja_tick = 3

    # czy moze sie przesunac?
    if (gra.mapa[nowa_koparka_y][nowa_koparka_x] == 0) or gra.kopanie_tick == 0:
        gra.koparka_y=nowa_koparka_y
        gra.koparka_x=nowa_koparka_x
        gra.kopanie_tick = 5 #reset kopania

    # kopanie
    if (gra.kopanie_kierunek == poprzedni_kierunek_kopania):
        gra.kopanie_tick-=1


    # wykopalismy cos?
    if gra.mapa[gra.koparka_y][gra.koparka_x] == 10:
        gra.urobek+=1

    # umiesc koparke
    gra.mapa[gra.koparka_y][gra.koparka_x] = 2

    # koniec paliwa
    if gra.paliwo < 0:
        # gra.mapa[gra.koparka_y][gra.koparka_y] = 0
        gra.zgon()
        
    

def rysuj(ekran, mapa, gry, grafika):
    for y in range(len(mapa)):
        for x in range(len(mapa[y])):
            element = mapa[y][x]
            if element == 1:
                pygame.draw.rect(ekran, pygame.Color(128,128,128,128), (x*7, y*7, 7, 7))
            if element == 2:
                pygame.draw.rect(ekran, pygame.Color(200,200,200,200), (x*7, y*7, 6, 6))
            if element == 10:
                pygame.draw.rect(ekran, pygame.Color(255,204,0,200), (x*7, y*7, 7, 7))
    for gra in gry:
        #statystyki
        ekran.blit(grafika.font.render(str(gra.paliwo), True, gra.kolor),(gra.baza_x*7,10))
        ekran.blit(grafika.font.render(str(gra.urobek)+" / "+str(gra.wynik), True, gra.kolor),(gra.baza_x*7,25))
        ekran.blit(grafika.font.render(str(gra.klocek), True, gra.kolor),(gra.baza_x*7,40))
        #baza
        r,g,b=gra.kolor
        pygame.draw.rect(ekran, pygame.Color(r,g,b,10), (gra.baza_x*7, gra.baza_y*7, 6, 6))

        #sterowanie myszka
        if isinstance(gra.sterowanie,SterowanieMyszka):
            pygame.draw.rect(ekran, pygame.Color(100,100,100,10), MYSZKA_GORA, 1)
            pygame.draw.rect(ekran, pygame.Color(100,100,100,10), MYSZKA_LEWO, 1)
            pygame.draw.rect(ekran, pygame.Color(100,100,100,10), MYSZKA_PRAWO, 1)
            pygame.draw.rect(ekran, pygame.Color(100,100,100,10), MYSZKA_DOL, 1)


    



def glowna():
    pygame.init() # startuje silnik

    gra1 = Gra((100,200,100), SterowanieKlawiatura())
    # gra2 = Gra((100,100,200), SterowanieMyszka())
    # gra2 = Gra((100,100,200), SterowaniePadem())
    gra2 = Gra((100,100,200), SterowanieKlawiatura(K_a,K_d,K_w,K_s,K_TAB))



    grafika = Grafika()


    ekran = pygame.display.set_mode((700, 700)) # inicjalizujemy ekran z jego wielkoscia 700 na 700 pikseli

    czy_dziala = True # ta zmienna kontroluje czy program dalej dziala
    while czy_dziala:
        ekran.fill((255, 255, 255)) # wyczyszczenie ekranu

        gra1.mapa = gra2.mapa
        przelicz(gra1)
        gra2.mapa = gra1.mapa
        przelicz(gra2)
        
        rysuj(ekran, gra2.mapa, [gra1,gra2], grafika) # wyrysuj wszystko co ma byc narysowane

        pygame.display.update()

        # obsluga klikniecia zamkniecia okna
        for zdarzenie in pygame.event.get():
            if zdarzenie.type == pygame.QUIT:
                czy_dziala = False
            
        time.sleep(0.1) # opoznienie, FPS'y

    pygame.quit() # zamkniecie pygame


if __name__ == "__main__":
    glowna()